#!/usr/bin/env bash
#./phrase.sh --count 5 --phrase "hello world" --reverse
# Counts - Number of times to print phrase
count=5

# Phrase - Message to print
phrase="hello world"

# Reverse - Whether to reverse string
reverse=0

# Parse options
while [[ $# -gt 0 ]]; do
   key="$1"
   case $key in
     -c|--count)
        count="$2"
        shift
        ;;
     -p|--phrase)
        phrase="$2"
        shift
        ;;
     -r|--reverse)
        reverse=1
        shift
        ;;
    esac
    shift
done

# Generate phrase
for ((i=0; i<$count; i++)); do

  # Reverse phrase if reverse flag set
  if [ $reverse -eq 1 ]; then
    rev_phrase=$(echo $phrase | rev)
    echo $rev_phrase
  else
    echo $phrase
  fi

done